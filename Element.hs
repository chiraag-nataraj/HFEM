{-# LANGUAGE Trustworthy #-}

module Element(createElement) where

import Datatypes
import Common
import MaterialModel()

import qualified Data.List as L
import qualified Data.Matrix as M

{-# SPECIALIZE createElement :: NeoHookean -> [Properties] -> Nodes -> ShapeFunctionDerivs -> Weights -> IPs -> Displacements -> Element #-}

createElement :: MaterialModel a => a
              -> [Properties]
              -> Nodes
              -> ShapeFunctionDerivs
              -> Weights
              -> IPs
              -> Displacements
              -> Element

createElement m ps nodes dndξs ws ips u =
  let dofs = length u `div` length nodes
      ident = [[δ i j | j <- indrange] | i <- indrange]
      mx = (flip (gendfdx dndξs) . L.transpose . map coords) nodes
      fx = zipWith (zipWith (+)) ident . (flip (gendfdx dndξs) . L.transpose . separateDOFs dofs) u
      φx = matmul fx mx
      bx = matmul fx (L.transpose . fx)
      k = calcStiffness m ps dndξs bx φx ws ips
      r = calcResidual m ps dndξs bx φx ws ips
  in
    Element m k r nodes bx

gendfdx :: ShapeFunctionDerivs -> [Double] -> [[Double]] -> [[Double]]
gendfdx dndξs x = map (\v -> map (\dndξ -> sum (zipWith (*) (map ($x) dndξ) v)) dndξs)

calcStiffness :: MaterialModel a => a
              -> [Properties]
              -> ShapeFunctionDerivs
              -> ([Double] -> [[Double]])
              -> ([Double] -> [[Double]])
              -> Weights
              -> IPs
              -> Stiffness
calcStiffness m ps dndξs bx φx w ips b i a k =
  let integrand =
        map (\ip ->
               let bmat = (M.fromLists . bx) ip
                   dσijdbkqbql j l = sum (map (\q -> modulus ps m bmat i j k q * M.getElem q l bmat) indrange)
                   dσijdbpkbpl j l = sum (map (\p -> modulus ps m bmat i j p k * M.getElem p l bmat) indrange)
                   σij = stress ps m bmat i
                   (φinv,ζ) = calcmmat φx ip
                   dnadξ = extract ip a dndξs
                   dnbdξ = extract ip b dndξs
                   dnady = ddy dnadξ φinv
                   dnbdy = ddy dnbdξ φinv
                   dnadyk = (M.getElem 1 1 . dnady) k
                   dnbdyk = (M.getElem 1 1 . dnbdy) k
                   ugh = sum (map (\j -> sum (map (\l -> let { dnadyl = (M.getElem 1 1 . dnady) l; dnbdyj = (M.getElem 1 1 . dnbdy) j } in ζ * dnadyl * dnbdyj * (dσijdbkqbql j l + dσijdbpkbpl j l)) indrange)) indrange)
                   ugh2 = sum (map (\j -> let { dnbdyj = (M.getElem 1 1 . dnbdy) j; dnadyj = (M.getElem 1 1 . dnady) j } in ζ * σij j * (dnbdyj * dnadyk - dnbdyk * dnadyj)) indrange)
               in
                 ugh + ugh2)
        ips
  in
    ((sum .) . zipWith (*)) integrand w

calcResidual :: MaterialModel a => a
             -> [Properties]
             -> ShapeFunctionDerivs
             -> ([Double] -> [[Double]])
             -> ([Double] -> [[Double]])
             -> Weights
             -> IPs
             -> Force
calcResidual m ps dndξs bx φx w ips b i =
  let integrand =
        map (\ip ->
               let σij = (stress ps m . M.fromLists . bx) ip i
                   (φinv,ζ) = calcmmat φx ip
                   dnbdξ = extract ip b dndξs
                   dnbdy = ddy dnbdξ φinv
                   ugh = sum (map (\j -> -ζ * σij j * (M.getElem 1 1 . dnbdy) j) indrange)
               in
                 ugh)
        ips
  in
    ((sum .) . zipWith (*)) integrand w

calcmmat :: ([Double] -> [[Double]]) -> [Double] -> (M.Matrix Double, Double)
calcmmat mx ip =
  let mmat = (M.fromLists . mx) ip
      mdet = det3x3 mmat
      minv = minv3x3 mmat mdet
  in
    (minv,mdet)

