# HFEM
A Haskell FEM implementation

This is a very extensible implementation of FEM in Haskell with auto-parallelization (using the `-N` RTS option). As of right now, this is in alpha stage and many bugs are still being worked out!

# Features

* Easily add new material models (just implement the `MaterialModel` typeclass)
* Easily add new element types (just provide the shape function derivatives, integration (quadrature) points, and quadrature weights)
* Parallelized assembly function

# TODO

* Figure out whether results are correct — seems okay for simple problems, but gives suspicious results for more complicated ones
* Expose number of threads as a user option or build it in rather than forcing the user to use the RTS option
* Locate and reduce sudden spike in memory usage