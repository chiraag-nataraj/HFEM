{-# LANGUAGE Trustworthy #-}

module MaterialModel() where

import Datatypes
import Common

import qualified Data.Matrix as M

instance MaterialModel NeoHookean where
  stress p _ b i j =
    let muchk = findProperty p shear
        kchk = findProperty p bulk
    in
      case (muchk,kchk) of
        (Just (Properties μ1), Just (Properties k1)) ->
          let fdet = (sqrt . det3x3) b
              bkk = M.trace b
          in
            (value μ1 / (fdet ** (5/3))) * (M.getElem i j b - (bkk / 3) * δ i j) +
            value k1 * (fdet - 1) * δ i j
        _ -> error "Required material properties not found; Need μ and K"
  modulus p _ b i j k l =
    let muchk = findProperty p shear
        kchk = findProperty p bulk
    in
      case (muchk,kchk) of
        (Just (Properties μ1), Just (Properties k1)) ->
          let bdet = det3x3 b
              fdet = sqrt bdet
              bkk = M.trace b
              binv = minv3x3 b bdet
          in
            (-5 * value μ1 / (6 * fdet ** (5/3))) * M.getElem l k binv *
            (M.getElem i j b - (bkk / 3) * δ i j) +
            (value μ1 / (fdet ** (5/3))) * (δ i k * δ j l - (1/3) * δ i j * δ k l) +
            (value k1 * fdet / 2) * δ i j * M.getElem l k binv
        _ -> error "Required material properties not found; Need μ and K"
