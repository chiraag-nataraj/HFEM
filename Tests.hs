module Tests where

import Test.HUnit
import Common
import Datatypes
import Element

props = [Properties (Bulk 250),Properties (Shear 0.3)]

testCSTElem1 :: Displacements -> Element
testCSTElem1 = createElement
               NeoHookean
               props
               [Node 0 [1,0,-1]
               ,Node 1 [0,1,-1]
               ,Node 2 [0,0,-1]
               ,Node 3 [1,0,1]
               ,Node 4 [0,1,1]
               ,Node 5 [0,0,1]]
               dndξcst
               wscst
               ipscst

testCSTElem2 :: Displacements -> Element
testCSTElem2 = createElement
               NeoHookean
               props
               [Node 0 [1,0,-1]
               ,Node 6 [1,1,-1]
               ,Node 1 [0,1,-1]
               ,Node 3 [1,0,1]
               ,Node 7 [1,1,1]
               ,Node 4 [0,1,1]]
               dndξcst
               wscst
               ipscst

nm1elem = [(0,[(0,0)])
          ,(1,[(0,1)])
          ,(2,[(0,2)])
          ,(3,[(0,3)])
          ,(4,[(0,4)])
          ,(5,[(0,5)])]

nm1elemt = nodemap [testCSTElem1 undefined]

nm2elem = [(0,[(1,0),(0,0)])
          ,(1,[(1,2),(0,1)])
          ,(2,[(0,2)])
          ,(3,[(1,3),(0,3)])
          ,(4,[(1,5),(0,4)])
          ,(5,[(0,5)])
          ,(6,[(1,1)])
          ,(7,[(1,4)])]

nm2elemt = nodemap [testCSTElem1 undefined, testCSTElem2 undefined]

testCSTnm1elem = TestCase (assertEqual "for one element," nm1elem nm1elemt)

testCSTnm2elem = TestCase (assertEqual "for two elements," nm2elem nm2elemt)

nmtests = TestList [TestLabel "One CST element nodemap" testCSTnm1elem
                   ,TestLabel "Two CST elements nodemap" testCSTnm2elem]

pf1elem = [[[((0,0),0)],[((0,1),0)],[((0,2),0)],[((0,3),0)],[((0,4),0)],[((0,5),0)]]
          ,[[((1,0),0)],[((1,1),0)],[((1,2),0)],[((1,3),0)],[((1,4),0)],[((1,5),0)]]
          ,[[((2,0),0)],[((2,1),0)],[((2,2),0)],[((2,3),0)],[((2,4),0)],[((2,5),0)]]
          ,[[((3,0),0)],[((3,1),0)],[((3,2),0)],[((3,3),0)],[((3,4),0)],[((3,5),0)]]
          ,[[((4,0),0)],[((4,1),0)],[((4,2),0)],[((4,3),0)],[((4,4),0)],[((4,5),0)]]
          ,[[((5,0),0)],[((5,1),0)],[((5,2),0)],[((5,3),0)],[((5,4),0)],[((5,5),0)]]]

pf2elem = [[[((0,0),0),((0,0),1)]
           ,[((0,1),0),((0,2),1)]
           ,[((0,2),0)]
           ,[((0,3),0),((0,3),1)]
           ,[((0,4),0),((0,5),1)]
           ,[((0,5),0)]
           ,[((0,1),1)]
           ,[((0,4),1)]]
          ,[[((1,0),0),((2,0),1)]
           ,[((1,1),0),((2,2),1)]
           ,[((1,2),0)]
           ,[((1,3),0),((2,3),1)]
           ,[((1,4),0),((2,5),1)]
           ,[((1,5),0)]
           ,[((2,1),1)]
           ,[((2,4),1)]]
          ,[[((2,0),0)]
           ,[((2,1),0)]
           ,[((2,2),0)]
           ,[((2,3),0)]
           ,[((2,4),0)]
           ,[((2,5),0)]
           ,[]
           ,[]]
          ,[[((3,0),0),((3,0),1)]
           ,[((3,1),0),((3,2),1)]
           ,[((3,2),0)]
           ,[((3,3),0),((3,3),1)]
           ,[((3,4),0),((3,5),1)]
           ,[((3,5),0)]
           ,[((3,1),1)]
           ,[((3,4),1)]]
          ,[[((4,0),0),((5,0),1)]
           ,[((4,1),0),((5,2),1)]
           ,[((4,2),0)]
           ,[((4,3),0),((5,3),1)]
           ,[((4,4),0),((5,5),1)]
           ,[((4,5),0)]
           ,[((5,1),1)]
           ,[((5,4),1)]]
          ,[[((5,0),0)]
           ,[((5,1),0)]
           ,[((5,2),0)]
           ,[((5,3),0)]
           ,[((5,4),0)]
           ,[((5,5),0)]
           ,[]
           ,[]]
          ,[[((1,0),1)]
           ,[((1,2),1)]
           ,[]
           ,[((1,3),1)]
           ,[((1,5),1)]
           ,[]
           ,[((1,1),1)]
           ,[((1,4),1)]]
          ,[[((4,0),1)]
           ,[((4,2),1)]
           ,[]
           ,[((4,3),1)]
           ,[((4,5),1)]
           ,[]
           ,[((4,1),1)]
           ,[((4,4),1)]]]

pf1elemt = pairsf nm1elem
pf2elemt = pairsf nm2elem

testCSTpf1elem = TestCase (assertEqual "for one element," pf1elem pf1elemt)

testCSTpf2elem = TestCase (assertEqual "for one element," pf2elem pf2elemt)

pftests = TestList [TestLabel "One CST element pairsf" testCSTpf1elem
                   ,TestLabel "Two CST elements pairsf" testCSTpf2elem]
