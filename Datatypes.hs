{-# LANGUAGE GADTs,Trustworthy #-}

module Datatypes where

import qualified Data.Matrix as M
import qualified Numeric.LinearAlgebra.Data as LAD
import Data.Hashable

type ModulusTensor = Int -> Int -> Int -> Int -> Double
type Strain = M.Matrix Double
type Stress = Int -> Int -> Double

class Property a where
  value :: a -> Double
  name :: a -> String

data Properties where
  Properties :: Property a => a -> Properties

newtype Youngs = Youngs Double
newtype Poisson = Poisson Double
newtype Shear = Shear Double
newtype Bulk = Bulk Double

instance Property Youngs where
  value (Youngs i) = i
  name (Youngs _) = "youngs"

instance Property Poisson where
  value (Poisson i) = i
  name (Poisson _) = "poisson"

instance Property Shear where
  value (Shear i) = i
  name (Shear _) = "shear"

instance Property Bulk where
  value (Bulk i) = i
  name (Bulk _) = "bulk"

class MaterialModel a where
  stress :: [Properties] -> a -> Strain -> Stress
  modulus :: [Properties] -> a -> Strain -> ModulusTensor

data NeoHookean = NeoHookean

type NID = Int
type ElNum = Int
type ElNodeNum = Int

data Node = Node { nid :: NID, coords :: [Double] } deriving (Show,Eq)
type Nodes = [Node]

instance Hashable Node where
  hashWithSalt i x = i + nid x + hashWithSalt i (coords x)

type Displacements = [Double]

type ShapeFunction = [Double] -> Double
type ShapeFunctions = [ShapeFunction]
type ShapeFunctionDerivs = [[ShapeFunction]]

type Stiffness = Int -> Int -> Int -> Int -> Double
type Force = Int -> Int -> Double

type Weights = [Double]
type IPs = [[Double]]

data Element where
  Element :: MaterialModel a =>
    {
      mm :: a
    , elstiff :: Stiffness
    , elforce :: Force
    , elnode :: Nodes
    , elstrain :: [Double] -> [[Double]]
    } -> Element

instance Show Element where
  show (Element _ _ _ nodes _) = "Element {elnode = " ++ show nodes ++ "}"

type GlobalStiffness = LAD.Matrix Double
type GlobalForce = LAD.Matrix Double

data DOFBC = DOFBC Int Int Double deriving Show

type Converged = Bool
type Diverged = Bool
type Iteration = Int

type PNode = (Int, [Double])
type PElement = (Int, [Int])
type PNodes = [PNode]
type PElements = [PElement]

type IElement = (Int, Nodes)
type IElements = [IElement]

type NodeData = [(Int,[Double])]
type ElementData = [(Int,[[Double]])]
