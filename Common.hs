{-# LANGUAGE Trustworthy #-}

module Common where

import Datatypes

import Control.Arrow
import qualified Control.Monad.Random as R
import Data.Function
import qualified Data.HashMap.Lazy as HM
import qualified Data.List as L
import qualified Data.Matrix as M
import qualified Data.Map as MM
import Data.Tuple

findProperty :: Property a => [Properties] -> a -> Maybe Properties
findProperty [] _ = Nothing
findProperty (l@(Properties x):xs) a | name a == name x = Just l
                                     | otherwise = findProperty xs a

indrange :: [Int]
indrange = [1..3]

δ :: (Eq a,Num b) => a -> a -> b
δ i j | i == j = 1
      | otherwise = 0

youngs :: Youngs
youngs = Youngs 1

poisson :: Poisson
poisson = Poisson 1

shear :: Shear
shear = Shear 1

bulk :: Bulk
bulk = Bulk 1

separateDOFs :: Int -> Displacements -> [Displacements]
separateDOFs _ [] = []
separateDOFs n x = take n x : separateDOFs n (drop n x)

forceDisplacements :: [DOFBC] -> Displacements -> Displacements
forceDisplacements = flip
                     (((head . M.toLists) .) .
                      foldr
                      (\(DOFBC node dof val) ->
                         M.setElem val (1,3 * node + dof)) .
                       M.fromLists . (:[]))

selectDisplacements :: M.Matrix Double -> Nodes -> Displacements
selectDisplacements = concatMap . (flip map indrange .) . (. ((+) . (*) 3 . nid)) . (.) . flip (M.getElem 1)

updateNodalCoords :: [Displacements -> Element] -> Displacements -> [Element]
updateNodalCoords es u =
  let nodes = map (\e -> (elnode . e) undefined) es
      umat = M.fromLists [u]
      eus = map (separateDOFs 3 . selectDisplacements umat) nodes
      upd = zipWith (zipWith (\n un -> n { coords = (zipWith (+) un . coords) n })) nodes eus
      elems = zipWith (\e n -> e (selectDisplacements umat n)) es nodes
  in
    zipWith (\e nnew -> e { elnode = nnew }) elems upd

elDisps :: [Displacements -> Element] -> Displacements -> [Element]
elDisps es u =
  let nodes = map (\e -> (elnode . e) undefined) es
      umat = M.fromLists [u]
  in
    zipWith (\e n -> e (selectDisplacements umat n)) es nodes

nodemap :: [Element] -> [(NID, [(ElNum, ElNodeNum)])]
nodemap es =
  let elnodes = map elnode es
  in
    (L.sort . HM.toList . HM.fromListWith (++)) (concatMap (\(x,y) -> zipWith (\w z -> (w,[z])) (map nid x) (zip (repeat y) [0..])) (zip elnodes [0..]))

pairsf :: [(NID, [(ElNodeNum, ElNum)])] -> [[[((ElNodeNum, ElNodeNum), ElNum)]]]
pairsf nm = map (\(_,x) -> map (\(_,y) -> map (swap . second (\[w,z] -> (w,z))) (MM.toList (MM.intersectionWith (++) (MM.fromList (map (second (:[])) x)) (MM.fromList (map (second (:[])) y))))) nm) nm

matmul :: ([Double] -> [[Double]]) -> ([Double] -> [[Double]]) -> [Double] -> [[Double]]
matmul f g = uncurry (flip (map . flip (map . (.) sum . zipWith (*)) . L.transpose . g) . f) . (id &&& id)

extract :: [Double] -> Int -> ShapeFunctionDerivs -> M.Matrix Double
extract = ((M.fromLists .) .) . (((: []) .) .) . (map .) . (. flip (!!)) . (.) . (&)

ddy :: M.Matrix Double -> M.Matrix Double -> Int -> M.Matrix Double
ddy a b c = a * M.colVector (M.getCol c b)

det3x3 :: M.Matrix Double -> Double
det3x3 m = M.getElem 1 1 m *
           (M.getElem 2 2 m * M.getElem 3 3 m -
            M.getElem 2 3 m * M.getElem 3 2 m) -
           M.getElem 1 2 m *
           (M.getElem 2 1 m * M.getElem 3 3 m -
            M.getElem 2 3 m * M.getElem 3 1 m) +
           M.getElem 1 3 m *
           (M.getElem 2 1 m * M.getElem 3 2 m -
            M.getElem 2 2 m * M.getElem 3 1 m)

minv3x3 :: M.Matrix Double -> Double -> M.Matrix Double
minv3x3 m d = M.fromLists
              [[(M.getElem 2 2 m * M.getElem 3 3 m -
                 M.getElem 2 3 m * M.getElem 3 2 m) / d
               ,(M.getElem 1 3 m * M.getElem 3 2 m -
                 M.getElem 1 2 m * M.getElem 3 3 m) / d
               ,(M.getElem 1 2 m * M.getElem 2 3 m -
                 M.getElem 1 3 m * M.getElem 2 2 m) / d]
              ,[(M.getElem 2 3 m * M.getElem 3 1 m -
                 M.getElem 2 1 m * M.getElem 3 3 m) / d
               ,(M.getElem 1 1 m * M.getElem 3 3 m -
                 M.getElem 1 3 m * M.getElem 3 1 m) / d
               ,(M.getElem 1 3 m * M.getElem 2 1 m -
                 M.getElem 1 1 m * M.getElem 2 3 m) / d]
              ,[(M.getElem 2 1 m * M.getElem 3 2 m -
                 M.getElem 2 2 m * M.getElem 3 1 m) / d
               ,(M.getElem 1 2 m * M.getElem 3 1 m -
                 M.getElem 1 1 m * M.getElem 3 2 m) / d
               ,(M.getElem 1 1 m * M.getElem 2 2 m -
                 M.getElem 1 2 m * M.getElem 2 1 m) / d]]

nξbrick :: ShapeFunctions
nξbrick =
  [\[x,y,z] -> (1 - x) * (1 - y) * (1 - z) / 8
  ,\[x,y,z] -> (1 + x) * (1 - y) * (1 - z) / 8
  ,\[x,y,z] -> (1 + x) * (1 + y) * (1 - z) / 8
  ,\[x,y,z] -> (1 - x) * (1 + y) * (1 - z) / 8
  ,\[x,y,z] -> (1 - x) * (1 - y) * (1 + z) / 8
  ,\[x,y,z] -> (1 + x) * (1 - y) * (1 + z) / 8
  ,\[x,y,z] -> (1 + x) * (1 + y) * (1 + z) / 8
  ,\[x,y,z] -> (1 - x) * (1 + y) * (1 + z) / 8]
  

dndξbrick :: ShapeFunctionDerivs
dndξbrick =
  [[\[_,y,z] -> -(1 - y) * (1 - z) / 8,\[_,y,z] -> (1 - y) * (1 - z) / 8
   ,\[_,y,z] -> (1 + y) * (1 - z) / 8,\[_,y,z] -> -(1 + y) * (1 - z) / 8
   ,\[_,y,z] -> -(1 - y) * (1 + z) / 8,\[_,y,z] -> (1 - y) * (1 + z) / 8
   ,\[_,y,z] -> (1 + y) * (1 + z) / 8,\[_,y,z] -> -(1 + y) * (1 + z) / 8]
  ,[\[x,_,z] -> -(1 - x) * (1 - z) / 8,\[x,_,z] -> -(1 + x) * (1 - z) / 8
   ,\[x,_,z] -> (1 + x) * (1 - z) / 8,\[x,_,z] -> (1 - x) * (1 - z) / 8
   ,\[x,_,z] -> -(1 - x) * (1 + z) / 8,\[x,_,z] -> -(1 + x) * (1 + z) / 8
   ,\[x,_,z] -> (1 + x) * (1 + z) / 8,\[x,_,z] -> (1 - x) * (1 + z) / 8]
  ,[\[x,y,_] -> -(1 - x) * (1 - y) / 8,\[x,y,_] -> -(1 + x) * (1 - y) / 8
   ,\[x,y,_] -> -(1 + x) * (1 + y) / 8,\[x,y,_] -> -(1 - x) * (1 + y) / 8
   ,\[x,y,_] -> (1 - x) * (1 - y) / 8,\[x,y,_] -> (1 + x) * (1 - y) / 8
   ,\[x,y,_] -> (1 + x) * (1 + y) / 8,\[x,y,_] -> (1 - x) * (1 + y) / 8]]

ipsbrick :: IPs
ipsbrick =
  [[-1 / sqrt 3, -1 / sqrt 3, -1 / sqrt 3]
  ,[1 / sqrt 3, -1 / sqrt 3, -1 / sqrt 3]
  ,[-1 / sqrt 3, 1 / sqrt 3, -1 / sqrt 3]
  ,[1 / sqrt 3, 1 / sqrt 3, -1 / sqrt 3]
  ,[-1 / sqrt 3, -1 / sqrt 3, 1 / sqrt 3]
  ,[1 / sqrt 3, -1 / sqrt 3, 1 / sqrt 3]
  ,[-1 / sqrt 3, 1 / sqrt 3, 1 / sqrt 3]
  ,[1 / sqrt 3, 1 / sqrt 3, 1 / sqrt 3]]

wsbrick :: Weights
wsbrick = [1, 1, 1, 1, 1, 1, 1, 1]

nξcst :: ShapeFunctions
nξcst =
  [\[x,_,z] -> x * (1 - z) / 2
  ,\[_,y,z] -> y * (1 - z) / 2
  ,\[x,y,z] -> (1 - x - y) * (1 - z) / 2
  ,\[x,_,z] -> x * (1 + z) / 2
  ,\[_,y,z] -> y * (1 + z) / 2
  ,\[x,y,z] -> (1 - x - y) * (1 + z) / 2]

dndξcst :: ShapeFunctionDerivs
dndξcst =
  [[\[_,_,z] -> (1 - z) / 2,const 0,\[_,_,z] -> -(1 - z) / 2
   ,\[_,_,z] -> (1 + z) / 2,const 0,\[_,_,z] -> -(1 + z) / 2]
  ,[const 0,\[_,_,z] -> (1 - z) / 2,\[_,_,z] -> -(1 - z) / 2
   ,const 0,\[_,_,z] -> (1 + z) / 2,\[_,_,z] -> -(1 + z) / 2]
  ,[\[x,_,_] -> -x / 2,\[_,y,_] -> -y / 2, \[x,y,_] -> -(1 - x - y) / 2
   ,\[x,_,_] -> x / 2, \[_,y,_] -> y / 2, \[x,y,_] -> (1 - x - y) / 2]]

ipscst :: IPs
ipscst = [[1 / 3, 1 / 3, -1 / sqrt 3],[1 / 3, 1 / 3, 1 / sqrt 3]]

wscst :: Weights
wscst = [1/2,1/2]

verifyTangent :: MaterialModel a => [Properties] -> a -> Double
verifyTangent ps mat =
  let perturb = 1e-8
      g = R.mkStdGen 1
      [i0,i1,i2,i3,i4,i5] = take 6 (R.randomRs (0,2) g)
      b0mat = M.fromLists [[1 + i0,i1,i2],[i1,1 + i3,i4],[i2,i4,1 + i5]]
      s0 = stress ps mat b0mat
  in
    abs (sum (concatMap
    (\i ->
       concatMap
       (\j ->
          concatMap
          (\k ->
             map
             (\l ->
                let b =
                      M.fromLists
                      [[M.getElem m n b0mat +
                        perturb * δ m k * δ n l
                       | n <- indrange]
                      | m <- indrange]
                    s = stress ps mat b
                in
                  (s i j - s0 i j) / perturb - modulus ps mat b0mat i j k l
             )
             indrange)
          indrange)
       indrange)
    indrange))

calcStrain :: IPs -> [Element] -> [[Strain]]
calcStrain ips = map (\e -> map (M.fromLists . elstrain e) ips)

calcStress :: MaterialModel a => [Properties] -> a -> IPs -> Weights -> [Element] -> [[Stress]]
calcStress ps m ips ws = map (\e -> map (\(w,ip) i j -> w * (stress ps m . M.fromLists . elstrain e) ip i j) (zip ws ips))

calcStressX :: MaterialModel a => [Properties] -> a -> Element -> [Double] -> Stress
calcStressX ps m = ((stress ps m . M.fromLists) .) . elstrain

genξscst :: [[Double]]
genξscst = [[x,y,z] | x <- [0,0.1..1], y <- [0,0.1..1 - x], z <- [-1,-0.9..1]]

genξsbrick :: [[Double]]
genξsbrick = [[x,y,z] | x <- [-1,-0.95..1], y <- [-1,-0.95..1], z <- [-1,-0.95..1]]

genfξ :: ShapeFunctions -> [Double] -> [[Double]] -> [Double]
genfξ naξs ξ = map (sum . zipWith (*) (map ($ξ) naξs))
