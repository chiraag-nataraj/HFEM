module Main where

import Assembly
import Common
import Datatypes
import Element

import Control.Arrow ((&&&),second)
import qualified Data.List as L
-- import qualified Data.Matrix as M
import Data.Maybe
import qualified Data.Vector as V
import Text.Parsec
import Text.Parsec.String

parseFloat :: Parser Double
parseFloat =
  do
    sign <- option "" (string "-")
    digits <- many1 digit
    decimal <- option '.' (char '.')
    f <- option ['0'] (many1 digit)
    fexp <- option "" (string "e" <|> string "E")
    expSign <- option "" (string "+" <|> string "-")
    fexponent <- option "" (many1 digit)
    return (read (sign ++ digits ++ [decimal] ++ f ++ fexp ++ expSign ++ fexponent) :: Double)
      <?> "floating-point number"

parseInt :: Parser Int
parseInt =
  do
    sign <- option "" (string "-")
    digits <- many1 digit
    return (read (sign ++ digits) :: Int)

parseNode :: Parser (Int, [Double])
parseNode =
  do
    nodeid <- parseInt
    _ <- many1 space
    point <- parseFloat `sepBy` string " "
    return (nodeid, point)

parseElement :: Parser (Int, [Int])
parseElement =
  do
    eid <- parseInt
    _ <- many1 space
    _ <- many digit
    _ <- many1 space
    num <- parseInt
    _ <- many1 space
    mapM_ (\_ -> do { _ <- digit; many1 space; }) [1..num]
    vertices <- parseInt `sepBy` string " "
    return (eid, vertices)

parseMSH :: Parser (PNodes, PElements)
parseMSH =
  do
    _ <- string "$MeshFormat\n"
    _ <- many (noneOf "\n")
    _ <- char '\n'
    _ <- string "$EndMeshFormat\n$Nodes\n"
    _ <- many (noneOf "\n")
    _ <- char '\n'
    nodes <- parseNode `sepEndBy` space
    _ <- string "$EndNodes\n$Elements\n"
    _ <- many (noneOf "\n")
    _ <- char '\n'
    elements <- parseElement `sepEndBy` space
    _ <- string "$EndElements"
    return (nodes, elements)

importMSH :: FilePath -> IO (PNodes, PElements)
importMSH f =
  parseFromFile parseMSH f >>=
  (\x -> 
     case x of
       Left err -> error (show err)
       Right m -> return m)

modNodes :: Double -> PNodes -> PNodes
modNodes t nodes =
  let δnid = length nodes
  in
    concatMap
    (\(nidi,ncoords) ->
        case ncoords of
          [x,y,z] -> [(nidi - 1,[x,y,z - t]),(nidi + δnid - 1,[x,y,z + t])]
          _ -> error "Unexpected number of coordinates") nodes

filterElements :: PElements -> PElements
filterElements = filter (\(_,y) -> case y of { [_,_,_] -> True; _ -> False })

renumberElements :: PElements -> PElements
renumberElements = zipWith (\x (_,enodes) -> (x,enodes)) [1..]

modElements :: Int -> PElements -> PElements
modElements nnodes = map (\(eid,enodes) -> (eid,enodes ++ map (+nnodes) enodes))

createNodes :: PNodes -> Nodes
createNodes = map (uncurry Node)

createElements :: Nodes -> PElements -> [Displacements -> Element]
createElements ns es =
  let nsv = V.fromList ns
  in
    map (\(_,enodes) -> let nodes = map ((nsv V.!) . subtract 1) enodes in createElement NeoHookean props nodes dndξcst wscst ipscst) es

createPElements :: [Element] -> PElements
createPElements = zipWith ((curry . second) (map ((+1) . nid) . elnode)) [1..]

exportMSH :: (PNodes, PElements,NodeData,ElementData) -> FilePath -> IO ()
exportMSH (ns,es,nds,stresses) f =
  let header = "$MeshFormat\n2.2 0 8\n$EndMeshFormat\n"
      nnodes = length ns
      nelems = length es
      nodes = "$Nodes\n" ++ show nnodes ++ "\n" ++ unlines (map (\(nidi,ncoords) -> unwords (show nidi : map show ncoords)) ns) ++ "$EndNodes\n"
      elems = "$Elements\n" ++ show nelems ++ "\n" ++ unlines (map (\(eid,enodes) -> unwords ([show eid,"6","2","0","9"] ++ map show enodes)) es) ++ "$EndElements\n"
      nodedatas = "$NodeData\n1\n\"Displacements\"\n1\n0.0\n3\n0\n3\n" ++ show nnodes ++ "\n" ++ unlines (map (\(nidi,ndisps) -> unwords (show nidi : map show ndisps)) nds) ++ "$EndNodeData\n"
      elemdatas = "$ElementData\n1\n\"Stresses\"\n1\n0.0\n3\n0\n9\n" ++ show nelems ++ "\n" ++ unlines (map (\(eidi,estress) -> unwords (show eidi : concatMap (map show) estress)) stresses) ++ "$EndElementData\n"
  in
    writeFile f (header ++ nodes ++ elems ++ nodedatas ++ elemdatas)

uBC :: ([Double] -> Bool) -> Int -> Double -> Node -> Maybe DOFBC
uBC f dof val n | (f . coords) n = Just (DOFBC (nid n) dof val)
                | otherwise = Nothing

generateBCs :: Nodes -> [[Double] -> Bool] -> [Int] -> [Double] -> [DOFBC]
generateBCs ns = (((catMaybes . concat) .) .) . zipWith3 (((flip map ns .) .) . uBC)

props :: [Properties]
props = [Properties (Bulk 250), Properties (Shear 0.3)]

writeDat :: FilePath -> [[([Double],Double)]] -> IO ()
writeDat fo vals = writeFile fo (unlines (concatMap (map (\(c,v) -> unwords (map show c ++ [show v]))) vals))

importAndExtrude :: FilePath -> Double -> FilePath -> FilePath -> IO ()
importAndExtrude fi t fomsh fodat =
  do
    (ns,es) <- importMSH fi
    let nnodes = length ns
        (nns,nes) = ((L.sort
                      . modNodes t) ns
                    ,(L.sort
                      . modElements nnodes
                      . renumberElements
                      . filterElements) es)
        nodes = createNodes nns
        u0 = replicate (3 * length nodes) 0
        s = 0.5
        elems = createElements nodes nes
        bcs = generateBCs
              nodes
              [(s ==) . head . tail
              ,((-s) ==) . head . tail
              ,(s ==) . head
              ,((-t) ==) . head . tail . tail]
              [2,2,1,3]
              [5e-3,-5e-3,0,0]
        us = solveNewtonRaphson elems bcs u0
        newelems = updateNodalCoords elems us
        fns = (L.sort . L.nub . map (((+) 1 . nid) &&& coords) . concatMap elnode) newelems
        fes = createPElements newelems
        uds = (zip [1..] . separateDOFs 3) us
        stresses = map (\(eid,e) -> (eid,map (\i -> map (\j -> sum (map (\σip -> σip i j) e)) indrange) indrange))
                   (zip [1..]
                    (calcStress
                     props
                     NeoHookean
                     ipscst
                     wscst
                     newelems))
        σ22s = map (\(en,e) -> let {enodes = elnode en; xs = (L.transpose . map coords) enodes} in map (\ξ -> (genfξ nξcst ξ xs, calcStressX props NeoHookean e ξ 2 2)) genξscst) (zip newelems (elDisps elems us))
    putStrLn ("Number of elements: " ++ show (length elems))
    putStrLn ("Number of nodes: " ++ show (length nodes))
    exportMSH (fns,fes,uds,stresses) fomsh
    writeDat fodat σ22s
main :: IO ()
main = importAndExtrude "plate.msh" 1e-2 "test2.msh" "plate_crack.dat"
