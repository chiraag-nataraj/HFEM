CC="ghc"
CCGENARGS=-Wall -rtsopts -funbox-strict-fields -XUnboxedTuples -funfolding-use-threshold=100 -O3 -optc-O3 -j8 -threaded
CCPROFARGS=-rtsopts -prof -auto-all -caf-all -ddump-simpl -ddump-stranal -ddump-to-file -dppr-case-as-let
CCTHREADSCOPEARGS=-eventlog

all: bin

bin: *.hs
	ghc $(CCGENARGS) -o main Mesh.hs

oneelem : *.hs
	ghc $(CCGENARGS) -o oneelem Main.hs

clean:
	rm -f *.hi *.o *.dump-* *.prof *.hp *.tix *.pdf *.ps *.aux *.eventlog main oneelem

profiling: *.hs
	ghc $(CCGENARGS) $(CCPROFARGS) -o main Mesh.hs

threadscope: *.hs
	ghc $(CCGENARGS) $(CCTHREADSCOPEARGS) -o main Mesh.hs
