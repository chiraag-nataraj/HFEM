{-# LANGUAGE Trustworthy #-}

module Assembly(solveNewtonRaphson) where

import Datatypes
import Common
-- import Element

import Control.Parallel.Strategies
import Control.Arrow
import qualified Data.Foldable as F
import qualified Data.List as L
import qualified Data.Matrix as M
import qualified Data.Sequence as S
import qualified Data.Vector as V
import Debug.Trace
import qualified Numeric.LinearAlgebra as LA
import qualified Numeric.LinearAlgebra.Data as LAD

assembleElements :: [Element] -> [(NID, [(ElNum, ElNodeNum)])] -> [[[((ElNodeNum, ElNodeNum), ElNum)]]] -> (GlobalStiffness,GlobalForce)
assembleElements es nm pf =
  let elstiffs = map elstiff es
      elresids = map elforce es
      elstiffsvec = S.fromList elstiffs
      elresidsvec = S.fromList elresids
      gstiff x = (LAD.fromLists . F.toList . fmap F.toList . S.fromList) (concatMap (\row -> map (\i -> S.fromList (concatMap (\col -> parMap rdeepseq (\k -> L.foldl' (+) 0 (map (\((b,a),e) -> S.index elstiffsvec e b i a k) col)) indrange) row)) indrange) x)
      gforce :: [(NID, [(ElNum, ElNodeNum)])] -> GlobalForce
      gforce x = (LAD.tr . LAD.fromLists . (:[])) (concatMap (\row -> parMap rdeepseq (\i -> ((. snd) . (L.foldl' (+) 0 .)) (map (\(e,b) -> S.index elresidsvec e b i)) row) indrange) x)
      gk = gstiff pf
      gf = gforce nm
  in
    (gk,gf)

-- pairsf :: [(NID, [(ElNodeNum, ElNum)])] -> [[[((ElNodeNum, ElNodeNum), ElNum)]]]
-- pairsf x = map (\(_,zls) -> map (\(_,wls) -> zip (zip (map fst zls) (map fst wls)) (map snd zls `L.intersect` map snd wls)) x) x

applyBCs :: [DOFBC] -> (GlobalStiffness,GlobalForce) -> (GlobalStiffness,GlobalForce)
applyBCs bcs (k0,f0) =
  let kv = (V.fromList . map V.fromList . LAD.toLists) k0
      fv = (V.fromList . map V.fromList . LAD.toLists) f0
      rows = map (\(DOFBC node dof _) -> (3 * node + dof - 1,V.fromList [δ i (3 * node + dof - 1) | i <- [0..length kv - 1]])) bcs
      cols = [map (\(DOFBC node dof _) -> (3 * node + dof - 1,δ i (3 * node + dof - 1))) bcs | i <- [0..length kv - 1]]
      vals = map (\(DOFBC node dof _) -> (3 * node + dof - 1,V.singleton 0)) bcs
      krows = zipWith (V.//) ((V.toList . (V.//) kv) rows) cols
      frows = (V.//) fv vals
      kn = (LAD.fromLists . map V.toList) krows
      fn = (LAD.fromLists . map V.toList . V.toList) frows
  in
    (kn,fn)

solveNewtonRaphson :: [Displacements -> Element] -> [DOFBC] -> Displacements -> Displacements
solveNewtonRaphson es bcs =
  let iter :: Converged -> Diverged -> Iteration -> Displacements -> Displacements
      iter c d i u | c = forceDisplacements bcs u
                   | i >= 1000 = error ("Exceeded max iterations (1000): " ++ show u)
                   | d = error "Seems to have diverged"
                   | otherwise =
                     let ub = forceDisplacements bcs u
                         umat = M.fromLists [ub]
                         elems = map (\e -> e (selectDisplacements umat ((elnode . e) undefined))) es
                         nm = nodemap elems
                         pf = pairsf nm
                         (kn,fn) = applyBCs bcs (assembleElements elems nm pf)
                         du = maybe (error "Singular!") (head . L.transpose . LAD.toLists) (LA.linearSolve kn fn)
                         tostop = (uncurry ((sum .) . zipWith (*)) . (id &&& id)) du
                     in
                       trace (show (i,tostop)) (iter (tostop <= 1e-2) (tostop >= 10) (i + 1) (zipWith (+) ub du))
  in
    iter False False 0
